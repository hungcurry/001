import { useRoutes } from 'react-router-dom'
// ===================
// ... page ...
// ===================
import Home from '@/pages/Home.tsx'

//System Test
import BaseTest from '@/pages/BaseTest.tsx'

const routes = [
  {
    path: '/',
    element: <Home />,
  },
  // ------
  {
    path: '/baseTest',
    element: <BaseTest />,
  },
]

const AppRoutes: React.FC = () => {
  return useRoutes(routes)
}
export default AppRoutes
